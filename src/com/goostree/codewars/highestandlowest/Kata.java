package com.goostree.codewars.highestandlowest;

import java.util.Arrays;

public class Kata {
	public static String HighAndLow(String numbers) {
	    String[] numArray = numbers.split(" ");
	    int lowest = Integer.valueOf(numArray[0]);
		int highest =  Integer.valueOf(numArray[0]);
		
		for(int i = 1; i < numArray.length; i++) {
			int current = Integer.valueOf(numArray[i]);
			
			if(current > highest) {
				highest = current;
			}
			
			if(current < lowest) {
				lowest = current;
			}
		}
		
		/* Java 8
		Arrays.stream(numbers.split(" "))
				.mapToInt(i -> Integer.parseInt(i))
				.min()
				.getAsInt();
		*/
		
	    return highest + " " + lowest;
	}
}
